var User = require('../models/user');

module.exports = {

    show: function (req, res) {
        User.findById(req.params.id, function (err, user) {
            if (user) {
                res.json(user);
            } else {
                res.status(404).end();
            }
        });
    },

    create: function (req, res) {
        User.create({
            name: req.body.name
        }, function (err, user) {
            res.json(user);
        });
    }

};