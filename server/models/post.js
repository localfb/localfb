var mongoose    = require('mongoose');
var Schema      = mongoose.Schema;
var deepPopulate = require('mongoose-deep-populate');

var postSchema = new Schema({
    user: { type: Schema.Types.ObjectId, ref: 'User' },

    text: { type: String },
    author: { type: String },

    pictures: [{ type: Schema.Types.ObjectId, ref: 'Picture'}],
    comments: [{ type: Schema.Types.ObjectId, ref: 'Comment' }],
    likes: [{ type: Schema.Types.ObjectId, ref: 'Like'}],

    createdAt: { type: Date, default: Date.now }
});

postSchema.plugin(deepPopulate);

module.exports = mongoose.model('Post', postSchema);