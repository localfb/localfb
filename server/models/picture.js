var mongoose    = require('mongoose');
var Schema      = mongoose.Schema;

var pictureSchema = new Schema({
    post: { type: Schema.Types.ObjectId, ref: 'Post' },
    user: { type: Schema.Types.ObjectId, ref: 'User' },

    path: { type: Schema.Types.Mixed },

    pictures: [{ type: Schema.Types.ObjectId, ref: 'Picture' }],

    createdAt: { type: Date, default: Date.now }
});

module.exports = mongoose.model('Picture', pictureSchema);