var papercut = require('papercut');

papercut.configure(function () {
    papercut.set('storage', 'file');
    papercut.set('directory', './uploads');
    papercut.set('url', '/images');
});

exports.AvatarUploader = papercut.Schema(function (schema){
    schema.version({
        name: 'avatar',
        size: '320x320',
        process: 'crop'
    });

    schema.version({
        name: 'thumbnail',
        size: '40x40',
        process: 'crop'
    });
});

exports.PictureUploader = papercut.Schema(function (schema) {
    schema.version({
        name: 'original',
        process: 'copy'
    });

    schema.version({
        name: 'medium',
        size: '512x512',
        process: 'resize'
    });
});