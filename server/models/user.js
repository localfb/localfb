var mongoose    = require('mongoose');
var Schema      = mongoose.Schema;

var userSchema = new Schema({
    name: { type: String },

    posts: [{ type: Schema.Types.ObjectId, ref: 'Post'}],
    comments: [{ type: Schema.Types.ObjectId, ref: 'Comment' }],
    likes: [{ type: Schema.Types.ObjectId, ref: 'Like'}],
    avatar: { type: Schema.Types.ObjectId, ref: 'Picture', default: null},

    createdAt: { type: Date, default: Date.now }
});

module.exports = mongoose.model('User', userSchema);