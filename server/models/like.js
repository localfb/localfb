var mongoose    = require('mongoose');
var findOrCreate = require('mongoose-findorcreate');
var Schema      = mongoose.Schema;

var likeSchema = new Schema({
    post: { type: Schema.Types.ObjectId, ref: 'Post' },
    user: { type: Schema.Types.ObjectId, ref: 'user' },
    author: { type: String },

    createdAt: { type: Date, default: Date.now }
});

likeSchema.plugin(findOrCreate);

module.exports = mongoose.model('Like', likeSchema);