angular.module('localfb')
    .config(function ($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise("/posts");

        $stateProvider
            .state('posts', {
                url: "/posts",
                templateUrl: "components/posts/postsView.html",
                controller: "PostsCtrl"
            })
            .state('new-post', {
                url: "/posts/new",
                templateUrl: "components/posts/newPostView.html",
                controller: "PostsCtrl"
            })
            .state('new-user', {
                url: "/users/new",
                templateUrl: "components/users/newUserView.html",
                controller: "UsersCtrl"
            });
    });