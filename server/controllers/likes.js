var mongoose = require('mongoose');
var User = require('../models/user');
var Post = require('../models/post');
var Like = require('../models/like');

module.exports = {

    index: function (req, res) {
        Post.findById(req.params.id).populate('likes').exec(function (err, doc) {
            var options = {
                path: 'likes.user',
                model: 'User',
                select: 'name'
            };

            Post.populate(doc, options, function (err, post) {
                res.json(post.likes);
            });
        });
    },

    create: function (req, res) {
        User.findById(req.userId, function (err, user) {
            if (user) {
                Post.where({ _id: mongoose.Types.ObjectId(req.params.id) }).findOne().exec(function (err, post) {
                    if (post) {
                        Like.findOrCreate({
                            post: post._id,
                            user: user._id
                        }, function (err, like, created) {
                            if (created) {
                                post.likes.push(like._id);
                                post.save();
                                user.likes.push(like._id);
                                user.save();
                            }

                            res.json(like);
                        });
                    }
                });
            }
        });
    }

};