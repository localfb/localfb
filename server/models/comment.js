var mongoose    = require('mongoose');
var Schema      = mongoose.Schema;
var deepPopulate = require('mongoose-deep-populate');

var commentSchema = new Schema({
    post: { type: Schema.Types.ObjectId, ref: 'Post' },
    user: { type: Schema.Types.ObjectId, ref: 'User' },
    text: { type: String },

    createdAt: { type: Date, default: Date.now }
});

commentSchema.plugin(deepPopulate);

module.exports = mongoose.model('Comment', commentSchema);