angular.module('localfb').controller('UsersCtrl', function($scope, $localStorage, $state, Restangular) {
    $scope.newUser = {
        name: null
    };

    $scope.init = function () {
        if ($localStorage.user) {
            $state.go('posts');
        }
    };

    $scope.createUser = function (e) {
        var postBody = {
            name: $scope.newUser.name
        };

        console.log($scope.newUser);

        Restangular.all('users').post(postBody).then(function (user) {
            $localStorage.user = {
                id: user._id,
                name: user.name
            };

            Restangular.setDefaultHeaders({'X-USER-ID': $localStorage.user.id});
            $state.go('posts');
        });
    };

    $scope.init();
});