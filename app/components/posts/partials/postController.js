angular.module('localfb').controller('PostCtrl', function($scope, Restangular) {
    $scope.newComment = {};

    $scope.createComment = function () {
        Restangular.one('posts', $scope.post._id).all('comments').post($scope.newComment).then(function (comment) {
            $scope.post.commentsLoaded.unshift(comment);
            $scope.post.comments.unshift(comment._id);
            $scope.newComment = {};
        });
    };

    $scope.loadComments = function () {
        $scope.post.showComments = !$scope.post.showComments;

        if ($scope.post.showComments) {
            Restangular.one('posts', $scope.post._id).all('comments').getList().then(function (comments) {
                $scope.post.commentsLoaded = comments;
            });
        }
    };

    $scope.likePost = function () {
        if (!$scope.post.liked) {
            Restangular.one('posts', $scope.post._id).all('likes').post().then(function (like) {
                $scope.post.likes.push(like);
                $scope.post.liked = true;
            });
        }
    };
});