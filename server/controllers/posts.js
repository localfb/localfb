var User = require('../models/user');
var Like = require('../models/like');
var Post = require('../models/post');

module.exports = {

    index: function (req, res) {
        var postsQuery = Post.find().lean().deepPopulate('user.avatar pictures').sort('-createdAt');

        postsQuery.limit(req.query.limit || 20);

        postsQuery.skip(req.query.offset || 0);

        postsQuery.exec(function (err, posts) {
            User.findById(req.userId, function (err, user) {
                if (user) {
                    for (var i=0; i< posts.length; i++) {
                        posts[i].liked = commonElement(user.likes, posts[i].likes);
                    }
                }
                res.json(posts);
            });
        });
    },

    create: function (req, res) {
        User.findById(req.userId, function (err, user) {
            if (user) {
                Post.create({
                    user: user._id,
                    text: req.body.text
                }, function (err, post) {
                    user.posts.push(post._id);
                    user.save(function (err) {
                        res.json(post);
                    });
                });
            }
        });
    }

};

function commonElement(a, b) {
    var ai=0, bi=0;
    var result = false

    while( ai < a.length && bi < b.length )
    {
        if      (a[ai] < b[bi] ){ ai++; }
        else if (a[ai] > b[bi] ){ bi++; }
        else /* they're equal */
        {
            result = true;
            break;
        }
    }

    return result;
}