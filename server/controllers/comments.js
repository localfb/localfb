var User = require('../models/user');
var Post = require('../models/post');
var Comment = require('../models/comment');

module.exports = {

    index: function (req, res) {
        Post.findById(req.params.id).populate('comments', null, null, { sort: { 'createdAt': -1 } }).deepPopulate('comments.user comments.user.avatar').exec(function (err, post) {
            res.json(post.comments);
        });
    },

    create: function (req, res) {
        User.findById(req.userId, function (err, user) {
            if (user) {
                Post.findById(req.params.id, function (err, post) {
                    if (post) {
                        Comment.create({
                            post: post._id,
                            user: user._id,
                            text: req.body.text
                        }, function (err, comment) {
                            post.comments.push(comment._id);
                            post.save();
                            user.comments.push(comment._id);
                            user.save();

                            Comment.findOne(comment).deepPopulate('user.avatar').exec(function (err, comment) {
                                res.json(comment);
                            });
                        });
                    }
                });
            }
        });
    }

};