// dependencies
var express     = require('express');
var bodyParser  = require('body-parser');
var mongoose    = require('mongoose');
var multer      = require('multer');

// initialization/configuration
var app = express();
app.use(bodyParser.json());
mongoose.connect('mongodb://localhost/localfb');
app.use(function (req, res, next) {
    req.userId = req.headers['x-user-id'] ? req.headers['x-user-id'] : null;
    next();
});
app.use(multer());

// controllers
var posts    = require('./controllers/posts');
var comments = require('./controllers/comments');
var likes    = require('./controllers/likes');
var users    = require('./controllers/users');
var uploads  = require('./controllers/uploads');

// routes
var apiRouter = express.Router();

apiRouter.get('/posts', posts.index);
apiRouter.post('/posts', posts.create);

apiRouter.get('/posts/:id/likes', likes.index);
apiRouter.post('/posts/:id/likes', likes.create);

apiRouter.get('/posts/:id/comments', comments.index);
apiRouter.post('/posts/:id/comments', comments.create);

apiRouter.post('/posts/:id/pictures', uploads.picture);

apiRouter.get('/users/:id', users.show);
apiRouter.post('/users', users.create);

apiRouter.post('/user/avatar', uploads.avatar);

app.use('/api', apiRouter);

app.use('/images', express.static(__dirname + '/../uploads'));
app.use('/bower_components', express.static(__dirname + '/../bower_components'));
app.use(express.static(__dirname + '/../app'));

// startup
app.listen(8080, function () {
    console.log("Server up and running on port 8080...");
});