var papercut = require('../services/papercut');
var Post = require('../models/post');
var User = require('../models/user');
var Picture = require('../models/picture');

module.exports = {

    picture: function (req, res) {
        var uploader = new papercut.PictureUploader();

        Post.findById(req.params.id, function (err, post) {
            if (post) {
                uploader.process(req.files.file.name.replace(/[.].+$/gm, ""), req.files.file.path, function (err, images) {
                    if (images) {
                        Picture.create({
                            post: post._id,
                            path: images
                        }, function (err, picture) {
                            post.pictures.push(picture._id);
                            post.save();
                            res.json(picture);
                        });
                    }
                });
            } else {
                res.status(404).end();
            }
        });
    },

    avatar: function (req, res) {
        var uploader = new papercut.AvatarUploader();

        User.findById(req.userId, function (err, user) {
            if (user && req.files.avatar) {
                uploader.process(req.files.avatar.name.replace(/[.].+$/gm, ""), req.files.avatar.path, function (err, images) {
                    if (images) {
                        Picture.create({
                            user: user._id,
                            path: images
                        }, function (err, picture) {
                            user.avatar = picture._id;
                            user.save();
                            res.json(picture);
                        });
                    }
                });
            }
        });
    }

};