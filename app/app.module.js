angular.module('localfb', ['ui.router', 'ionic', 'restangular', 'flow', 'angularMoment', 'ngStorage'])
    .config(function(RestangularProvider) {
        RestangularProvider.setBaseUrl('/api');
    })
    .run(function (Restangular, $localStorage) {
        if($localStorage.user) {
            Restangular.setDefaultHeaders({'X-USER-ID': $localStorage.user.id});
            Restangular.one('users', $localStorage.user.id).get().then(null, function () {
                delete $localStorage.user;
                window.location = '/';
            });
        }
    });