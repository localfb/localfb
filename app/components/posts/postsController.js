angular.module('localfb').controller('PostsCtrl', function($scope, Restangular, $state, $localStorage) {
    $scope.posts = [];
    $scope.newPost = {};

    $scope.getAllPosts = function () {
        Restangular.all('posts').getList().then(function (posts) {
            $scope.posts = posts;
            $scope.$broadcast('scroll.refreshComplete');
        });
    };

    $scope.doRefresh = function () {
        $scope.getAllPosts();
    };

    $scope.createPost = function (e) {
        $scope.newPost.author = $localStorage.author;
        Restangular.all('posts').post($scope.newPost).then(function (post) {
            e.opts.target = '/api/posts/' + post._id + '/pictures';
            e.upload();
        });
    };

    $scope.goToPosts = function (){
        $state.go('posts');
    };

    $scope.loadMore = function () {
        var queryParams = {
            offset: $scope.posts.length
        };

        Restangular.all('posts').getList(queryParams).then(function (posts) {
            $scope.posts.concat(posts);
            $scope.$broadcast('scroll.infiniteScrollComplete');
        });
    };
});