angular.module('localfb').controller('localfbCtrl', function ($scope, $localStorage) {
    $scope.init = function () {
        if(!$localStorage.user) {
            console.log("NO USER");
            window.location = '#/users/new';
        }
    };

    $scope.init();
});